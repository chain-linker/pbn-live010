---
layout: post
title: "왓챠 고평점 스릴러 영화 BEST 3 + 개인 평가"
toc: true
---

 왓챠는 개인별로 영화나 도서, TV프로그램의 별점을 매기고 형씨 데이터를 통해 개인에게 으레 맞는 작품을 찾아낼 핵 있는 플랫폼 입니다. 수많은 영화매니아 분들이 이용하는 서비스이고, 강우 최고의 평론가로 알려진 이동진님도 활발히 이용중이십니다. 그렇게 평점에 대한 신뢰로가 몹시 높은 편이며, 마이너한 장르의 영화들에 대한 다양한 평가를 확인할 행복 있습니다.
 

 성제무두 참말로 왓챠를 통해 약 1,500편의 영화 평점을 기록해왔습니다. 이빨 글에서는 왓챠에서 3.5점 이상을 기록하고 있는 스릴러 영화 3편을 추천하려고 합니다. 비두 제출물로 보고, 개인적으로도 재밌게 본 추천작들 입니다. 왓챠에서 별점 3.5점 이상은 평단과 관객들의 생각이 일통 하는 수준의 명작들이며, 왓챠에는 알바와 같은 평점 조작을 찾기 힘들기 그리하여 객관적인 면에서 천만 높은 수준의 영화라고 할 수 있습니다.
 

 

## 세븐 (왓챠 평점 4.1점, 1995년)
 명배우인 브래드피트, 모건 프리먼이 주연으로 출연한 1995년의 영화 입니다. 감독은 데이빗 핀처로, '벤자민 버튼의 시간을 거꾸로 간다', '소셜 네트워크' 등의 흥행작으로도 유명한 단속 입니다.

 

 

 영화의 주요 줄거리는
 은퇴를 앞둔 관록의 형사인 윌리엄 소머셋(모건 프리먼)과 새로 전근 온 신참내기 형사 밀스(브래드 피트)가 팀이 된 바로 다음날, 강압에 의해 위가 찢어질 때까지 먹다가 죽은 초고도 비만 남자와 역시 강압에 의해 식칼로 자기 살을 베어내 죽은 악덕 변호사의 사건과 마주한다. ‘식탐’, ‘탐욕’… 그리고 ‘나태’, ‘분노’, ‘교만’, ‘욕정’. ‘시기’ 윌리엄 소머셋은 현장에 남은 흔적들로 기나긴 연쇄 살인이 시작되었음을 직감하고 성서의 7가지 죄악을 따라 발생하는 사건들을 추적하기 시작하는데… 가장 치밀한 일곱 개의 연쇄살인이 시작된다!
 

 저는 이익금 영화를 요마적 넷플릭스를 통해서 관람했는데요. 의심할 여지가 없는 두 배우의 연기와 몰입도를 높이는 데이빗 핀처의 세련된 연출이 눈길을 사로잡습니다. 무려 15년 전에 만든 영화임에도 애오라지 촌스러움이 없지요.

 

 일개인 평점 4.0
 

## 모스트 원티드 맨 (왓챠 평점 3.7점, 2014년)
 영화 마스터, 매그놀리아, 미션임파서블3, 헝거게임 등의 다양한 영화에서 명연기를 뽐낸 '필립 세이모어 호프만'의 압미 유작입니다. 영화를 본 이들이 말하는 대부분의 평가는 '마지막 까지 위대한 연기를 완성한 필립 세이모어 호프만'입니다.

 

 

 영화의 중대 줄거리는

 

 독일 최고의 스파이였으나 지금은 정보부 소속 비밀조직의 수장인 군터 바흐만(필립 세이모어 호프만). 정보원을 미끼 삼아 한결 큰 목표물을 제거하는 데 탁월한 재능을 가진 그의 앞에 흥미로운 먹잇감, ‘이사’가 나타난다. 인터폴 지명수배자인 이사는 아버지의 유산을 찾기 위해 함부르크로 밀항한 무슬림 청년. 본능적으로 이사를 쫓기 시작한 군터는 이사를 돕고 있는 인권 변호사 애너벨 리히터(레이첼 맥아덤스)와 유산을 관리하는 은행장 토마스 브루(윌렘 데포)의 존재를 알게 되고, 두 사람을 자신의 정보원으로 섭외하는 데 성공한다. 그리고는 이사를 이용해 테러리스트들의 자금줄로서 각국 정보부의 용의선상에 오른 닥터 압둘라를 체포할 은밀한 작전을 설계하는데...
 

 일사인 평점 3.5
 

 

## 데자뷰 (왓챠 평점 3.6점, 2006년)
 영화 맨 온 파이어, 에너미 오브 스테이트, 가일층 팬 [왓챠](https://imgresizing.com/entertain/post-00002.html) 등 명작 액션 스릴러를 만들어낸 토니 스콧 감독의 2006년 제품 입니다. 개봉 당시에 경로 수작으로 유명세를 떨쳤던 영화 입니다. 제목에서 처럼, 우리가 극한 번씩 경험하는 '데자뷰' 현상을 사용하고 있습니다. 호흡이 무진 빠르고, 몰입도가 높으며, 머리싸움이 즐거운 명작이라고 할 명맥 있습니다.
 

 

 영화의 주 컨셉은
 

 누구나 그런 경험을 한번쯤 해봤을것이다. 아무개 사람을 만났는데 왠지 예전부터 알던 놈 같다든지, 모 장소에 생전 본초 갔는데 그곳이 더없이 낯이 익다든지 하는... 우리가 자주 데자뷰라고 일컫는 과실 현상이 만약, 단순한 착각이 아니라면 어떨까? 여혹 과거로 부터 온 모모 경고라면?
 

 일개인 평점 4.0

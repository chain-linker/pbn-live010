---
layout: post
title: "[청약] 경기 힐스테이트 평택 더퍼스트 아파트 분양가 및 평면도 총정리"
toc: true
---

 본 공고는 청약홈에 2021년 12월 17일 입주자 모집 공고일 기준으로 작성한 내용입니다.

 자세한 내용은 청약홈의 청약일정 및 통계 게시판에서 확인하시면 되겠습니다.
 

 본 게시물은 어떠한 광고료를 받지 않고 순전히 공유하기 위해 작성했습니다.
 

 또한, 본문의 오류로 인한 책임은 뒷바라지 않습니다. 자세한 내용은 공고문을 상세히 읽어보시기 바랍니다.
 

 

## 힐스테이트 평택 더퍼스트 개요

### 위치
 경기도 평택시 서정동 780번지 일원

### 규모
 아파트 지하 3층에서 세정 최고 15층 17개 동, 총 1,107세대 중앙 일반분양 698세대

### 지불 주택형
 59㎡, 74㎡A/B, 76㎡, 84㎡A/B/C/D

### 입주 예정
 2024년 2월 예정
 

 

## 입지 환경

### 교통
 송탄역
 송탄시외버스터미널
 평택지제역SRT
 수원발 KTX (예정)

### 교육
 지장초등학교
 송현초등학교
 라온중학교
 라온고등학교 등 도보거리 안심 학군

### 생활
 홈플러스
 롯데시네마
 전통시장
 부락산 둘레길
 문화공원
 서정공원 등 다양한 인생살이 인프라
 

 

## 분양 일정
 12월 27일 (월) 특단 지급 청약 신청
 12월 28일 (화) 일반 1순위(당해) 청약 신청
 12월 29일 (수) 매일반 1순위(기타) 청약 신청
 12월 30일 (목) 심상성 2순위 청약 신청
 1월 5일 (수) 당첨자 발표
 

 

 

## 지역 배치도

## 평면도

## 급여 대상
 059.9600 71세대
074.8600A 35세대
074.8600B 210세대
076.8900 81세대
084.8800A 133세대
084.9400B 13세대
084.9600C 44세대
084.9400D 111세대
 

 총 698세대 공급
 

## 지출 금액
 주택형 별 최고가 준거 급부 금액은 다음과 같습니다.
 

 059.9600 34,714만 원
074.8600A 42,454만 원
074.8600B 42,526만 원
076.8900 44,882만 원
084.8800A 48,733만 원
084.9400B 48,475만 원
084.9600C 48,639만 원
084.9400D 48,444만 원
 

 

## 일반공급 신청 자격
 입주자 모집공고일 (12월 17일) 현재
 

 평택시에 6개월 한도 거주하거나 6개월 미달 및 수도권(서울특별시, 경기도)에 거주하는
 

 만 19세 이상인 자식 또는
 

 세대주인 미성년자 중
 

 입주자저축 순위별 자격요건을 갖춘 자
 

 전용면적 85㎡이하 가점제 75%, 추첨제 25% 적용입니다.
 [평택브레인시티중흥](https://humiliate-simplistic.com/life/post-00079.html) 

 자세한 신청 조력 및 요건은 밑 내용을 참고하세요.
 

 

## 신혼부부 특별공급 신청자격
 신혼부부 특별공급의 신청 자격은 다음과 같습니다.
 

 입주자 모집공고일 현재
 

 평택시에 6개월 극소 거주하거나 6개월 미만 및 수도권(서울특별시, 경기도)에 거주하고
 

 혼구 기간이 7년 이내(혼인신고일 기준, 재혼 포함)인
 

 무주택세대 구성원으로서
 

 소득기준을 충족하는 자
 

 수입 기준은 전년도 도시근로자 가구원수별 월평균 득 기준의 140% 이하(맞벌이 160% 이하)입니다.
 

 자세한 내용은 기저 가경 또는 공고문을 참고하세요.
 

 

 신혼부부 특별공급 청약에 대해 궁금한 점은 근기 링크를 참고하세요.
 

 [청약] 2021년 신혼부부 특별공급 약조 한눈에 확인하기
 

## 생애최초 소득조건
 생애최초 특별공급 신청 주권 및 이 조건은 아래와 같습니다.
 

 입주자 모집공고일 현재
 

 평택시에 6개월 궁극 거주하거나 6개월 미만 및 수도권(서울특별시, 경기도)에 거주하고
 

 생애최초(세대에 속한 모든 자가 과시 주택을 소유한 사실이 없는 경우) 살림집 구매이며
 

 소득기준을 충족하는 자
 

 득 기준은 전년도 도시근로자 가구원수별 월평균 실리 기준의 160% 이하입니다.
 

 자세한 내용은 이하 회도 또는 공고문을 참고하세요.
 

 

 전년도 소득을 확인하는 방법은 내의 링크를 참고하세요.
 

 [청약] 2021년도 도시근로자 월평균보수액 목적 나의 날찍 확인하는 방법
 

 자세한 내용은 공고문 또는 홈페이지를 참고하세요.
 https://www.hillstate.co.kr/salesinfo/s_main_info_renew.aspx?apt_num=871&code_type=17&code_val=0&code_subVal=&hPAGE=1
